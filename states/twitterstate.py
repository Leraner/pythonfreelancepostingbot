from aiogram.dispatcher.filters.state import StatesGroup, State


class TwitterState(StatesGroup):
    P1 = State()
    P2 = State()


class DeleteTwitter(StatesGroup):
    D1 = State()
    D2 = State()
