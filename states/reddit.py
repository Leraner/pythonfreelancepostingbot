from aiogram.dispatcher.filters.state import StatesGroup, State


class Reddit(StatesGroup):
    P1 = State()
    P2 = State()
    P3 = State()


class DeleteReddit(StatesGroup):
    P1 = State()
    P2 = State()


class RedditTime(StatesGroup):
    P1 = State()
    P2 = State()
    P3 = State()
