from gino.schema import GinoSchemaVisitor

from .config import db_name, db_pass, db_user, host, stack_telegram_channels
from .models import db, Parlers, Theepochtimes, Breitbart, Twitter, Subreddit, AllTelegramChannels, \
    TelegramChannelsReddit, TelegramChannelsTwitter, PostedReddit


class DBCommands:
    async def get_parler(self, username):
        parler = await Parlers.query.where(Parlers.name == username).gino.first()
        return parler

    async def get_twitter(self, username):
        twitter = await Twitter.query.where(Twitter.name == username).gino.first()
        return twitter

    async def del_twitter(self, username):
        old_twitter = await Twitter.query.where(Twitter.username == username).gino.first()
        if old_twitter:
            twitter = await Twitter.delete.where(Twitter.username == username).gino.first()
        else:
            twitter = True
        return twitter

    async def get_all_twitter(self):
        all_twitter = await Twitter.query.gino.all()
        return all_twitter

    async def get_all_reddit(self):
        all_reddit = await Subreddit.query.gino.all()
        return all_reddit

    async def del_reddit(self, name):
        old_reddit = await Subreddit.query.where(Subreddit.name == name).gino.first()
        if old_reddit:
            reddit = await Subreddit.delete.where(Subreddit.name == name).gino.first()
        else:
            reddit = True
        return reddit

    async def create_all_telegram_channels(self, channels):
        if ',' in channels:
            output_channels = []
            channels = channels.split(',')
            channels = list(map(lambda x: x.replace(' ', ''), channels))
            for channel in channels:

                if '@' not in channel:
                    channel = f'@{channel}'

                old_channel = await AllTelegramChannels.query.where(AllTelegramChannels.name == channel).gino.first()

                if old_channel:
                    output_channels.append(channel)
                else:
                    new_channel = AllTelegramChannels()
                    new_channel.name = channel
                    await new_channel.create()
                    output_channels.append(new_channel)
                    stack_telegram_channels.append(channel)

            return output_channels
        else:
            old_channel = await AllTelegramChannels.query.where(AllTelegramChannels.name == channels).gino.first()

            if old_channel:
                return old_channel

            if '@' not in channels:
                channels = f'@{channels}'

            new_channel = AllTelegramChannels()
            new_channel.name = channels
            stack_telegram_channels.append(channels)
            await new_channel.create()
            return new_channel

    async def create_posted_reddit(self, post_id, telegram_channel):

        new_posted_reddit = PostedReddit()
        new_posted_reddit.post_id = post_id
        new_posted_reddit.telegram_channel = telegram_channel

        await new_posted_reddit.create()
        return new_posted_reddit

    async def create_telegram_channel_twitter(self, channel, twitter_name):
        old_channels1 = await TelegramChannelsTwitter.query.where(
            TelegramChannelsTwitter.twitter_name == twitter_name).gino.all()
        for old_channel in old_channels1:
            if old_channel.name == channel:
                return None

        new_channel = TelegramChannelsTwitter()
        new_channel.name = channel
        new_channel.twitter_name = twitter_name
        await new_channel.create()
        return new_channel

    async def create_telegram_channel_reddit(self, channel, subreddit, posts_time):
        old_channels1 = await TelegramChannelsReddit.query.where(
            TelegramChannelsReddit.subreddit_name == subreddit).gino.all()
        for old_channel in old_channels1:
            if old_channel.name == channel:
                return None

        new_channel = TelegramChannelsReddit()
        new_channel.name = channel
        new_channel.time = posts_time
        new_channel.subreddit_name = subreddit
        await new_channel.create()
        return new_channel

    async def create_reddit(self, name, id):
        old_reddit = await Subreddit.query.where(Subreddit.subreddit_id == id).gino.first()

        if old_reddit:
            return old_reddit

        new_reddit = Subreddit()
        new_reddit.name = name
        new_reddit.subreddit_id = id

        await new_reddit.create()
        return new_reddit

    async def create_theepochtimes(self, last_post_id):
        theepochtimes = Theepochtimes()
        theepochtimes.last_post_id = last_post_id

        await theepochtimes.create()
        return theepochtimes

    async def create_breitbart(self, last_post_id):
        new_breitbart = Breitbart()
        new_breitbart.last_post_id = last_post_id

        await new_breitbart.create()
        return new_breitbart

    async def create_parler(self, user, last_post_id):
        old_parler = await self.get_parler(username=user['users'][0]['username'])

        if old_parler:
            return old_parler

        new_parler = Parlers()
        new_parler.name = user['users'][0]['username']
        new_parler.parler_id = user['users'][0]['id']
        new_parler.last_post_id = last_post_id

        await new_parler.create()
        return new_parler

    async def create_twitter(self, user, last_post_id, channels):
        old_twitter = await self.get_twitter(username=user['name'])

        if old_twitter:
            return old_twitter

        new_twitter = Twitter()
        new_twitter.name = user['name']
        new_twitter.username = user['screen_name']
        new_twitter.twitter_id = user['id']
        new_twitter.last_post_id = last_post_id
        new_twitter.channels = channels

        await new_twitter.create()
        return new_twitter


async def create_db():
    await db.set_bind(f'postgresql+asyncpg://{db_user}:{db_pass}@{host}/{db_name}')
    db.gino: GinoSchemaVisitor
    # await db.gino.drop_all()
    await db.gino.create_all()


async def load_telegram_channels():
    all_channels = await AllTelegramChannels.query.gino.all()
    for channel in all_channels:
        if channel.name not in stack_telegram_channels:
            stack_telegram_channels.append(channel.name)
