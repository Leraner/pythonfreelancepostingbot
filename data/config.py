from pathlib import Path

from environs import Env
from parler import Parler

env = Env()
env.read_env()

# Bot
BOT_TOKEN = env.str("BOT_TOKEN")
ADMINS = env.list("ADMINS")
db_user = env.str("DB_USER")
db_pass = env.str("DB_PASS")
db_name = env.str('DB_NAME')
host = env.str('HOST')

# Parler
jst = env.str('JST')
mst = env.str('MST')

client = Parler(mst=mst, jst=jst)

# Dirs
BASE_DIR = Path(__file__).parent
MEDIA_DIR = BASE_DIR / 'media'


# Channel
telegram_channel = env.str('CHANNEL')
stack_telegram_channels = []
tel_channels = []
del_channels = []

# Reddit
client_id = env.str('CLIENT_ID')
client_secret = env.str('SECRET_KEY')
username = env.str('USERNAME')
password = env.str('PASSWORD')
