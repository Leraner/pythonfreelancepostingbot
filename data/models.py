from gino import Gino
from sqlalchemy import (
    Column, Integer, BigInteger, String, Sequence, ForeignKey
)
from sqlalchemy import sql
from sqlalchemy.orm import relationship

db = Gino()


class AllTelegramChannels(db.Model):
    """Модель всех телеграм каналов"""
    __tablename__ = 'AllTelegramChannels'
    id = Column(Integer, Sequence('alltelegramchannels_id_seq'), unique=True, primary_key=True)
    name = Column(String, nullable=False)
    query: sql.Select


class TelegramChannelsTwitter(db.Model):
    """Модель телеграм каналов"""
    __tablename__ = 'TelegramChannelsTwitter'
    id = Column(Integer, Sequence('telegramchannelstwitter_id_seq'), unique=True, primary_key=True)
    name = Column(String, nullable=False)
    time = Column(Integer, default=None)
    twitter_name = Column(String, ForeignKey('Twitter.username'), unique=False)
    query: sql.Select


class TelegramChannelsReddit(db.Model):
    """Модель телеграм каналов"""
    __tablename__ = 'TelegramChannelsReddit'
    id = Column(Integer, Sequence('telegramchannelsreddit_id_seq'), unique=True, primary_key=True)
    name = Column(String, nullable=False)
    time = Column(Integer, default=2)
    last_post_id = Column(String, default=None)
    subreddit_id = Column(String, default=None)
    subreddit_name = Column(String, ForeignKey('Subreddit.name'), unique=False)
    reddit_posted = relationship('PostedReddit', backref='postedreddit', lazy='dynamic')
    query: sql.Select


class PostedReddit(db.Model):
    __tablename__ = 'PostedReddit'
    id = Column(Integer, Sequence('postedreddit_id_seq'), unique=True, primary_key=True)
    post_id = Column(Integer)
    telegram_channel = Column(Integer, ForeignKey('TelegramChannelsReddit.id'), unique=False)
    query: sql.Select


class Subreddit(db.Model):
    """Модель сабредитов"""
    __tablename__ = 'Subreddit'
    id = Column(Integer, Sequence('subreddit_id_seq'), unique=True, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    subreddit_id = Column(String)
    last_post_id = Column(String)
    telegram_channel = relationship('TelegramChannelsReddit', backref='reddit', lazy='dynamic')
    query: sql.Select


class Parlers(db.Model):
    """Модель парлер аккаунтов"""
    __tablename__ = 'Parler'
    id = Column(Integer, Sequence('parler_id_seq'), unique=True, primary_key=True)
    name = Column(String, nullable=False)
    parler_id = Column(String)
    last_post_id = Column(String)
    query: sql.Select


class Theepochtimes(db.Model):
    __tablename__ = 'Theepochtimes'
    id = Column(Integer, Sequence('theepochtimes_id_seq'), unique=True, primary_key=True)
    last_post_id = Column(Integer)
    query: sql.Select


class Breitbart(db.Model):
    __tablename__ = 'Breitbart'
    id = Column(Integer, Sequence('breitbart_id_seq'), unique=True, primary_key=True)
    last_post_id = Column(Integer)
    before_last_post_id = Column(Integer, nullable=True)
    query: sql.Select


class Twitter(db.Model):
    __tablename__ = 'Twitter'
    id = Column(Integer, Sequence('twitter_id_seq'), unique=True, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    username = Column(String, nullable=False, unique=True)
    telegram_channel = relationship('TelegramChannelsTwitter', backref='twitter', lazy='dynamic')
    twitter_id = Column(BigInteger)
    last_post_id = Column(BigInteger)
    query: sql.Select
