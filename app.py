import asyncio

from aiogram import executor

from data.database import create_db, load_telegram_channels
from handlers.admins.breitbart import parsing_breitbart
from handlers.admins.reddit import reddit_parser
from handlers.admins.theepochtimes import parsing_theepochtimes
from handlers.admins.twitter import twitter
from loader import dp
from utils.notify_admins import on_startup_notify
import subprocess


async def on_startup(dispatcher):
    await on_startup_notify(dispatcher)
    # await asyncio.sleep(5)
    await create_db()
    subprocess.call('chmod 755 alembic.sh', shell=True)
    subprocess.call(['./alembic.sh'], shell=True)
    await load_telegram_channels()
    # await asyncio.sleep(10)



async def periodic(sleep_for):
    while True:
        await asyncio.sleep(sleep_for)
        await reddit_parser()
        await twitter()
        await parsing_theepochtimes('https://www.theepochtimes.com/c-us-politics')
        await parsing_breitbart('https://www.breitbart.com/politics/')


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(periodic(20))

    executor.start_polling(dp, on_startup=on_startup, loop=loop, skip_updates=True)
