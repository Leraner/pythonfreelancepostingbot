from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

menu = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='/help'),
            KeyboardButton(text='/alltwitter'),
            KeyboardButton(text='/deltwitter'),
            KeyboardButton(text='/addtwitter')
        ],
        [
            KeyboardButton(text='/addreddit'),
            KeyboardButton(text='/delreddit'),
            KeyboardButton(text='/allreddit'),
            KeyboardButton(text='/changereddittime')
        ],
        [
            KeyboardButton(text='/check'),
            KeyboardButton(text='/createtg'),
        ]
    ],
    resize_keyboard=True
)
