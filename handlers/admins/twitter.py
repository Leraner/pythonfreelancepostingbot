import logging
import os
import time

import requests
from pytwitterscraper import TwitterScraper

from data.config import MEDIA_DIR, telegram_channel
from data.models import Twitter, TelegramChannelsTwitter
from loader import dp


async def twitter():
    try:
        tw = TwitterScraper()
        twitter_accounts = await Twitter.query.gino.all()
        for account in twitter_accounts:
            date = []
            last_post = None
            current_account = tw.get_profile(id=account.twitter_id).__dict__
            id = current_account['id']
            tweets = tw.get_tweets(id=id, count=2).__dict__['contents']

            for post in tweets:
                date.append(post['created_at'])

            date.sort()
            last_date = date[-1]

            for post in tweets:
                if post['created_at'] == last_date:
                    last_post = post

            if last_post['id'] != account.last_post_id:
                body = last_post['text']
                text = [
                    f'{account.name}',
                    f'{body}',
                ]
                await Twitter.update.values(last_post_id=last_post['id']).where(
                    Twitter.twitter_id == account.twitter_id).gino.first()

                account.last_post_id = last_post['id']

                if last_post['media']:
                    photo = last_post['media'][0]['image_url']
                    photo_name = photo.rsplit('/')[-1]
                    photo = requests.get(photo)
                    with open(f'{MEDIA_DIR}/{photo_name}', 'wb') as f:
                        f.write(photo.content)
                        f.close()

                    all_twitter_channels = await TelegramChannelsTwitter.query.gino.all()
                    for twitter_channel in all_twitter_channels:
                        await dp.bot.send_photo(twitter_channel.name, photo=open(f'{MEDIA_DIR}/{photo_name}', 'rb'),
                                                caption="\n".join(text), parse_mode='HTML')

                    os.remove(f'{MEDIA_DIR}/{photo_name}')
                else:
                    all_twitter_channels = await TelegramChannelsTwitter.query.gino.all()
                    for twitter_channel in all_twitter_channels:
                        await dp.bot.send_message(twitter_channel, text="\n".join(text),
                                                  disable_web_page_preview=True)



    except KeyboardInterrupt:
        logging.info('Twitter выключен')
    except:
        time.sleep(5)
