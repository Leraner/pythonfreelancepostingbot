import asyncio
import collections
import logging
import os
from datetime import datetime

import requests

from data import database
from data.config import MEDIA_DIR
from data.models import TelegramChannelsReddit, PostedReddit
from loader import reddit, dp

db = database.DBCommands()


async def reddit_parser():
    # try:
        all_subreddit = await TelegramChannelsReddit.query.gino.all()
        for subreddit_from_db in all_subreddit:
            all_posted_reddit = await PostedReddit.query.where(
                PostedReddit.telegram_channel == subreddit_from_db.id).gino.all()

            all_posts = []
            for post in all_posted_reddit:
                all_posts.append(post.post_id)

            x = int(len(all_posts) - len(all_posts) * 0.8)
            all_posts = all_posts[x::]

            reddit_time = subreddit_from_db.time

            if reddit_time is None:
                reddit_time = 2
                subreddit_from_db.update(time=2).apply()

            subreddit = reddit.subreddit(subreddit_from_db.subreddit_name)
            posts = {}

            if subreddit_from_db.subreddit_id is None:
                await subreddit_from_db.update(subreddit_id=subreddit.wiki.subreddit.id).apply()

            for submission in subreddit.hot(limit=200):
                dt = datetime.fromtimestamp(submission.created)
                dt = dt.strftime('%H')
                if int(dt) <= reddit_time:
                    posts.update({submission: submission.score})

            sorted_posts = {}
            sorted_keys = sorted(posts, key=posts.get)

            for w in sorted_keys:
                sorted_posts[w] = posts[w]

            [last] = collections.deque(sorted_posts, maxlen=1)

            for key in sorted_keys:
                await asyncio.sleep(5)
                if int(key.created) not in all_posts:
                    last = key
                    all_posts.append(int(key.created))
                    await db.create_posted_reddit(post_id=int(last.created), telegram_channel=subreddit_from_db.id)
                    break

            if subreddit_from_db.last_post_id is None or int(last.created) != int(subreddit_from_db.last_post_id):

                await subreddit_from_db.update(last_post_id=str(int(last.created))).apply()

                subreddit_from_db.last_post_id = str(int(last.created))

                # Max length in telegram
                if len(last.title) + len(last.selftext) > 200:
                    last.selftext = f'{last.selftext[:-((len(last.title) + len(last.selftext) - 200) + 3):]}...'

                text = [
                    last.title,
                    ' ',
                    last.selftext,
                ]

                try:
                    if last.preview:
                        photo = last.preview['images'][0]['source']['url']
                        photo_name = photo.split('=')[-1]
                        photo = requests.get(photo)
                        with open(f'{MEDIA_DIR}/{photo_name}.png', 'wb') as f:
                            f.write(photo.content)
                            f.close()

                        await dp.bot.send_photo(subreddit_from_db.name,
                                                photo=open(f'{MEDIA_DIR}/{photo_name}.png', 'rb'),
                                                caption="\n".join(text))

                        os.remove(f'{MEDIA_DIR}/{photo_name}.png')


                except AttributeError:
                    await dp.bot.send_message(subreddit_from_db.name, text="\n".join(text),
                                              disable_web_page_preview=True, parse_mode='HTML')

    # except KeyboardInterrupt:
    #     logging.info('subreddit выключен')
    # except:
    #     await asyncio.sleep(5)
