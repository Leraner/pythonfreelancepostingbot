import logging

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.types import CallbackQuery
from pytwitterscraper import TwitterScraper

from data import database, config
from data.config import client, stack_telegram_channels, tel_channels, del_channels
from data.models import TelegramChannelsReddit, Subreddit, Twitter, TelegramChannelsTwitter
from keyboards.default import menu
from loader import dp, reddit
from states.parler import Parler
from states.reddit import Reddit, DeleteReddit, RedditTime
from states.telegram_channels import CreateTGChannels
from states.twitterstate import TwitterState, DeleteTwitter

db = database.DBCommands()


@dp.message_handler(user_id=config.ADMINS, commands='keyboard')
async def show_menu(message: types.Message):
    await message.answer('Вы открыли клавиатуру', reply_markup=menu)


@dp.message_handler(user_id=config.ADMINS, commands=['addparler'], state=None)
async def add_parler(message: types.Message):
    await message.answer('Введите, пожалуйста, username')
    await Parler.P1.set()


@dp.message_handler(user_id=config.ADMINS, state=Parler.P1)
async def get_username(message: types.Message, state: FSMContext):
    if '@' in message.text:
        username = message.text[1::]
    else:
        username = message.text

    user = client.userSearch(username=username)
    logging.info(user)
    last_post_id = client.getPostsOfUserId(userId=user['users'][0]['id'])[
        'posts'][0]['_id']
    logging.info(last_post_id)
    await db.create_parler(user=user, last_post_id=last_post_id)

    await state.finish()


@dp.message_handler(user_id=config.ADMINS, commands=['alltwitter'])
async def get_all_twitter_accounts(message: types.Message):
    text = [
        'Все твиттер аккаунты:',
    ]
    all_accounts = await db.get_all_twitter()
    for account in all_accounts:
        text.append(f'{account.name} (username: {account.username})')

    await message.answer('\n'.join(text))


@dp.message_handler(user_id=config.ADMINS, commands=['addtwitter'], state=None)
async def add_twitter(message: types.Message):
    await message.answer('Введите, пожалуйста, username')
    await TwitterState.P1.set()


@dp.message_handler(user_id=config.ADMINS, state=TwitterState.P1)
async def set_channel_for_twitter(message: types.Message, state: FSMContext):
    username = message.text
    await state.update_data(username=username)
    await message.answer('Выберите телеграм каналы')
    await twitter_create_buttons(message=message)
    await TwitterState.P2.set()


async def twitter_create_buttons(message: types.Message):
    keyboard = types.InlineKeyboardMarkup()
    for reduction in stack_telegram_channels:
        btn = types.InlineKeyboardButton(text=reduction,
                                         callback_data=f'twittertest {reduction}')
        keyboard.add(btn)

    btn1 = types.InlineKeyboardButton(text='Окей',
                                      callback_data='twittertest ok')

    btn2 = types.InlineKeyboardButton(text='Отмена',
                                      callback_data='twittertest no')

    keyboard.add(btn1, btn2)
    await dp.bot.send_message(message.chat.id, "Все телеграм каналы", reply_markup=keyboard)


@dp.callback_query_handler(text_contains='twittertest', state=TwitterState.P2)
async def create_twitter_channels(call: CallbackQuery, state: FSMContext):
    channel = call.data.rsplit(' ')[-1]

    if channel not in tel_channels:
        tel_channels.append(channel)

    if 'ok' in call.data:
        tel_channels.remove('ok')
        if len(tel_channels):
            await state.update_data(channels=tel_channels)
            await dp.bot.send_message(config.ADMINS[0], 'Сохранено')
            await create_twitter(state=state)
        else:
            await dp.bot.send_message(config.ADMINS[0], 'Нет телеграм каналов')
            await state.finish()
    if 'no' in call.data:
        await state.finish()
        await dp.bot.send_message(config.ADMINS[0], 'Отменено')
        return None


async def create_twitter(state: FSMContext):
    try:
        state_data = await state.get_data()
        username = state_data.get('username')
        channels = state_data.get('channels')

        if '@' in username:
            username = username[1::]
        else:
            username = username

        date = []
        last_post = None

        tw = TwitterScraper()

        user = tw.get_profile(username).__dict__
        logging.info(user)
        id = user['id']
        tweets = tw.get_tweets(id=id, count=2).__dict__['contents']

        for post in tweets:
            date.append(post['created_at'])

        date.sort()
        last_date = date[-1]

        for post in tweets:
            if post['created_at'] == last_date:
                last_post = post

        last_post_id = last_post['id']

        await db.create_twitter(user=user, last_post_id=last_post_id, channels=channels)

        for channel in channels:
            await db.create_telegram_channel_twitter(channel=channel, twitter_name=username)

        tel_channels.clear()

        await dp.bot.send_message(config.ADMINS[0], 'Twitter аккаунт добавлен')
    except:
        await dp.bot.send_message(config.ADMINS[0], 'Пользователь не найден')

    await state.finish()


@dp.message_handler(user_id=config.ADMINS, commands=['deltwitter'])
async def delete_twitter(message: types.Message):
    await message.answer('Введите channelname твиттер аккаунта')
    await DeleteTwitter.D1.set()


@dp.message_handler(state=DeleteTwitter.D1)
async def get_twitter_username(message: types.Message, state: FSMContext):
    channelname = message.text

    if '@' in channelname:
        channelname = channelname[1::]

    await state.update_data(channelname=channelname)
    await message.answer('Выберите телеграм каналы')
    await twitter_delete_buttons(message=message, channelname=channelname)
    await DeleteTwitter.D2.set()


async def twitter_delete_buttons(message, channelname):
    keyboard = types.InlineKeyboardMarkup()
    all_channels = await TelegramChannelsTwitter.query.where(
        TelegramChannelsTwitter.twitter_name == channelname).gino.all()
    for channel in all_channels:
        btn = types.InlineKeyboardButton(text=channel.name,
                                         callback_data=f'del {channel.name}')
        keyboard.add(btn)

    btn1 = types.InlineKeyboardButton(text='Окей',
                                      callback_data='delt ok')

    btn2 = types.InlineKeyboardButton(text='Отмена',
                                      callback_data='delt no')

    btn3 = types.InlineKeyboardButton(text='Удалить везде',
                                      callback_data='delt all')

    keyboard.add(btn1, btn2, btn3)
    await dp.bot.send_message(message.chat.id, f"Все телеграм каналы twitter аккаунта {channelname}",
                              reply_markup=keyboard)


@dp.callback_query_handler(text_contains='delt', state=DeleteTwitter.D2)
async def get_callback_twitter(call: CallbackQuery, state: FSMContext):
    channel = call.data.rsplit(' ')[-1]

    if channel not in del_channels:
        del_channels.append(channel)

    if 'ok' in call.data:
        del_channels.remove('ok')
        await state.update_data(channels=del_channels)
        await dp.bot.send_message(config.ADMINS[0], 'Сохранено')
        await delete_reddit_channels(state=state)
    elif 'no' in call.data:
        del_channels.remove('no')
        await state.finish()
        await dp.bot.send_message(config.ADMINS[0], 'Отменено')
        return None
    elif 'all' in call.data:
        del_channels.remove('all')
        data = await state.get_data()
        channelname = data.get('channelname')
        await TelegramChannelsTwitter.delete.where(TelegramChannelsTwitter.twitter_name == channelname).gino.all()
        await Twitter.delete.where(Twitter.username == channelname).gino.first()
        await dp.bot.send_message(config.ADMINS[0], f'Twitter {channelname} удалён со всех каналов')
        await state.finish()
        return None


async def delete_twitter_channels(state: FSMContext):
    try:
        data = await state.get_data()

        channelname = data.get('channelname')
        channels = data.get('channels')
        for channel in channels:
            tg_channels = await TelegramChannelsTwitter.query.where(TelegramChannelsTwitter.name == channel).gino.all()
            for tg_channel in tg_channels:
                if tg_channel.twitter_name == channelname:
                    await tg_channel.delete()
                    del_channels.clear()
        await dp.bot.send_message(config.ADMINS[0], 'Каналы удалены')
    except:
        await dp.bot.send_message(config.ADMINS[0], 'Ошибка при удалении twitter аккаунта')

    await state.finish()


# REDDIT

@dp.message_handler(user_id=config.ADMINS, commands=['delreddit'], state=None)
async def delete_reddit(message: types.Message):
    await message.answer('Введите название subreddit')
    await DeleteReddit.P1.set()


@dp.message_handler(state=DeleteReddit.P1)
async def get_channel(message: types.Message, state: FSMContext):
    red = message.text
    await state.update_data(red=red)
    await message.answer('Выберите телеграм каналы')
    await reddit_delete_buttons(message=message, red=red)
    await DeleteReddit.P2.set()


async def reddit_delete_buttons(message: types.Message, red):
    keyboard = types.InlineKeyboardMarkup()
    all_channels = await TelegramChannelsReddit.query.where(TelegramChannelsReddit.subreddit_name == red).gino.all()
    for channel in all_channels:
        btn = types.InlineKeyboardButton(text=channel.name,
                                         callback_data=f'del {channel.name}')
        keyboard.add(btn)

    btn1 = types.InlineKeyboardButton(text='Окей',
                                      callback_data='del ok')

    btn2 = types.InlineKeyboardButton(text='Отмена',
                                      callback_data='del no')

    btn3 = types.InlineKeyboardButton(text='Удалить везде',
                                      callback_data='del all')

    keyboard.add(btn1, btn2, btn3)
    await dp.bot.send_message(message.chat.id, f"Все телеграм каналы subreddit'a {red}", reply_markup=keyboard)


@dp.callback_query_handler(text_contains='del', state=DeleteReddit.P2)
async def get_callback_reddit(call: CallbackQuery, state: FSMContext):
    channel = call.data.rsplit(' ')[-1]

    if channel not in del_channels:
        del_channels.append(channel)

    if 'ok' in call.data:
        del_channels.remove('ok')
        await state.update_data(channels=del_channels)
        await dp.bot.send_message(config.ADMINS[0], 'Сохранено')
        await delete_reddit_channels(state=state)
    elif 'no' in call.data:
        del_channels.remove('no')
        await state.finish()
        await dp.bot.send_message(config.ADMINS[0], 'Отменено')
        return None
    elif 'all' in call.data:
        del_channels.remove('all')
        data = await state.get_data()
        red = data.get('red')
        await TelegramChannelsReddit.delete.where(TelegramChannelsReddit.subreddit_name == red).gino.all()
        await Subreddit.delete.where(Subreddit.name == red).gino.first()
        await dp.bot.send_message(config.ADMINS[0], f'Subreddit {red} удалён со всех каналов')
        await state.finish()
        return None


async def delete_reddit_channels(state: FSMContext):
    try:
        data = await state.get_data()

        red = data.get('red')
        channels = data.get('channels')
        for channel in channels:
            tg_channels = await TelegramChannelsReddit.query.where(TelegramChannelsReddit.name == channel).gino.all()
            for tg_channel in tg_channels:
                if tg_channel.subreddit_name == red:
                    await tg_channel.delete()
                    del_channels.clear()
        await dp.bot.send_message(config.ADMINS[0], 'Каналы удалены')
    except:
        await dp.bot.send_message(config.ADMINS[0], 'Ошибка при удалении subreddit')

    await state.finish()


@dp.message_handler(user_id=config.ADMINS, commands=['addreddit'], state=None)
async def add_reddit(message: types.Message):
    await message.answer('Введите название subreddit')
    await Reddit.P1.set()


@dp.message_handler(state=Reddit.P1)
async def get_reddit_channels(message: types.Message, state: FSMContext):
    subreddit = message.text
    await state.update_data(subreddit=subreddit)
    await message.answer('Введите до какого периода времени будут браться посты (в часах)')
    await Reddit.P2.set()


@dp.message_handler(state=Reddit.P2)
async def get_channels_reddit(message: types.Message, state: FSMContext):
    try:
        posts_time = int(message.text)
        await state.update_data(posts_time=posts_time)

        await message.answer('Выберите телеграм каналы')
        await reddit_buttons(message=message)
        await Reddit.P3.set()
    except:
        await message.answer('Ошибка при вводе времени')


async def reddit_buttons(message: types.Message):
    keyboard = types.InlineKeyboardMarkup()
    for reduction in stack_telegram_channels:
        btn = types.InlineKeyboardButton(text=reduction,
                                         callback_data=f'test {reduction}')
        keyboard.add(btn)

    btn1 = types.InlineKeyboardButton(text='Окей',
                                      callback_data='test ok')

    btn2 = types.InlineKeyboardButton(text='Отмена',
                                      callback_data='test no')

    keyboard.add(btn1, btn2)
    await dp.bot.send_message(message.chat.id, "Все телеграм каналы", reply_markup=keyboard)


@dp.callback_query_handler(text_contains='test', state=Reddit.P3)
async def test(call: CallbackQuery, state: FSMContext):
    channel = call.data.rsplit(' ')[-1]

    if channel not in tel_channels:
        tel_channels.append(channel)

    if 'ok' in call.data:
        tel_channels.remove('ok')
        if len(tel_channels):
            await state.update_data(channels=tel_channels)
            await dp.bot.send_message(config.ADMINS[0], 'Сохранено')
            await create_reddit(state=state)
        else:
            await dp.bot.send_message(config.ADMINS[0], 'Нет телеграм аккаунтов')
            await state.finish()
    if 'no' in call.data:
        await state.finish()
        await dp.bot.send_message(config.ADMINS[0], 'Отменено')
        return None


async def create_reddit(state: FSMContext):
    try:
        data = await state.get_data()

        red = data.get('subreddit')
        channels = data.get('channels')
        posts_time = data.get('posts_time')
        subreddit = reddit.subreddit(red)
        if subreddit.wiki.subreddit.id:
            await db.create_reddit(name=red, id=subreddit.wiki.subreddit.id)
            for channel in channels:
                await db.create_telegram_channel_reddit(channel=channel, subreddit=red, posts_time=posts_time)
                tel_channels.clear()
            await dp.bot.send_message(config.ADMINS[0], 'Новый subreddit создан')
    except:
        await dp.bot.send_message(config.ADMINS[0], 'Ошибка при создании subreddit')

    await state.finish()


@dp.message_handler(commands=['allreddit'])
async def all_reddit(message: types.Message):
    text = [
        'Все subreddit:',
    ]
    all_subreddit = await db.get_all_reddit()
    for subreddit in all_subreddit:
        text.append(f'{subreddit.name}')

    await message.answer('\n'.join(text))


@dp.message_handler(user_id=config.ADMINS, commands=['changereddittime'], state=None)
async def change_time(message: types.Message):
    await message.answer('Введите название subreddit')
    await RedditTime.P1.set()


@dp.message_handler(state=RedditTime.P1)
async def get_time(message: types.Message, state: FSMContext):
    subreddit = message.text
    await state.update_data(subreddit=subreddit)
    await message.answer('Введите время в часах')
    await RedditTime.P2.set()


@dp.message_handler(state=RedditTime.P2)
async def get_time_hours(message: types.Message, state: FSMContext):
    try:
        changed_time = message.text
        await state.update_data(changed_time=changed_time)
        await message.answer('Выберите телеграм канал')
        data = await state.get_data()
        name = data.get('subreddit')
        await reddit_change_buttons(message=message, name=name)
        await RedditTime.P3.set()
    except:
        await message.answer('Ошибка при вводе времени')


async def reddit_change_buttons(message: types.Message, name):
    keyboard = types.InlineKeyboardMarkup()
    channels = await TelegramChannelsReddit.query.where(
        TelegramChannelsReddit.subreddit_name == name).gino.all()
    for reduction in channels:
        btn = types.InlineKeyboardButton(text=reduction.name,
                                         callback_data=f'change {reduction.name}')
        keyboard.add(btn)

    btn1 = types.InlineKeyboardButton(text='Окей',
                                      callback_data='change ok')

    btn2 = types.InlineKeyboardButton(text='Отмена',
                                      callback_data='change no')

    keyboard.add(btn1, btn2)
    await dp.bot.send_message(message.chat.id, "Все телеграм каналы", reply_markup=keyboard)


@dp.callback_query_handler(text_contains='change', state=RedditTime.P3)
async def get_channels_callback(call: CallbackQuery, state: FSMContext):
    channel = call.data.rsplit(' ')[-1]

    if channel not in tel_channels:
        tel_channels.append(channel)

    if 'ok' in call.data:
        tel_channels.remove('ok')
        await state.update_data(channels=tel_channels)
        await dp.bot.send_message(config.ADMINS[0], 'Сохранено')
        await get_time_and_change(state=state)
    if 'no' in call.data:
        await state.finish()
        await dp.bot.send_message(config.ADMINS[0], 'Отменено')
        return None


async def get_time_and_change(state: FSMContext):
    try:
        data = await state.get_data()
        subreddit_name = data.get('subreddit')
        channels = data.get('channels')
        time = data.get('changed_time')
        all_channels = await TelegramChannelsReddit.query.where(
            TelegramChannelsReddit.subreddit_name == subreddit_name).gino.all()
        for channel in all_channels:
            if channel.name in channels:
                await channel.update(time=int(time)).apply()

        await dp.bot.send_message(config.ADMINS[0], f'Время изменено на {time} (в часах)')
    except:
        await dp.bot.send_message(config.ADMINS[0], 'Ошибка при изменении времени')

    await state.finish()


@dp.message_handler(user_id=config.ADMINS, commands=['check'])
async def check(message: types.Message):
    channels_info = {}
    text = [
        'Телеграм аккаунты'
    ]
    telegram_channels_reddit = await TelegramChannelsReddit.query.gino.all()
    telegram_channels_twitter = await TelegramChannelsTwitter.query.gino.all()
    for channel in telegram_channels_reddit:
        if channel.name in channels_info:
            channels_info[f'{channel.name}'].append(
                f'{channel.subreddit_name}({channel.time})')
        else:
            channels_info.update(
                {channel.name: [f'{channel.subreddit_name}({channel.time})']})

    for channel in telegram_channels_twitter:
        if channel.name in channels_info:
            channels_info[f'{channel.name}'].append(
                f'{channel.twitter_name}(Twitter)')
        else:
            channels_info.update(
                {channel.name: [f'{channel.twitter_name}(Twitter)']})

    for el in channels_info:
        text.append(f"{el} - {', '.join(channels_info[f'{el}'])}")

    if len(channels_info) == 0:
        await message.answer('Нет добавленных subreddit/twitter')
    else:
        await message.answer('\n'.join(text))


@dp.message_handler(user_id=config.ADMINS, commands=['createtg'], state=None)
async def create_telegram_channel(message: types.Message):
    await message.answer('Введите channelname телеграм канала')
    await message.answer('Пример: @channelname1, @channelname2')
    await CreateTGChannels.P1.set()


@dp.message_handler(state=CreateTGChannels.P1)
async def get_telegram_channels(message: types.Message, state: FSMContext):
    await db.create_all_telegram_channels(channels=message.text)
    await message.answer('Телеграм канал(ы) добавлен(ы)')
    await state.finish()
