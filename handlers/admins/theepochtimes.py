import asyncio
import logging
import os

import requests
from bs4 import BeautifulSoup

from data import database
from data.config import MEDIA_DIR, telegram_channel
from data.models import Theepochtimes
from loader import dp

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
    'Accept': '*/*'
}

db = database.DBCommands()


async def parsing_theepochtimes(url):
    try:
        last_post = await Theepochtimes.query.where(Theepochtimes.id == 1).gino.first()
        if last_post:
            response = requests.get(url=url, headers=headers)
            if response.status_code == 200:
                html = response.text
                soup = BeautifulSoup(html, 'html.parser')
                div = soup.find_all('div', attrs={'class': 'left_col'})[1]
                post = div.find_all('li', attrs={'class': 'post_list'})[0]
                post_id = int(len(post.text))
                if post_id != last_post.last_post_id:
                    href = post.contents[3].contents[1].contents[1].attrs['href']
                    photo = post.find_all('img')[1].attrs['src']
                    photo_name = photo.rsplit('/')[-1]
                    photo = requests.get(photo)
                    with open(f'{MEDIA_DIR}/{photo_name}', 'wb') as f:
                        f.write(photo.content)
                        f.close()
                    await Theepochtimes.update.values(last_post_id=post_id).where(
                        Theepochtimes.id == 1).gino.first()
                    text = [
                        post.text.replace('\n', '').replace('\t', ''),
                        f'<a href="{href}">SOURCE</a>'
                    ]
                    await dp.bot.send_photo(telegram_channel, photo=open(f'{MEDIA_DIR}/{photo_name}', 'rb'),
                                            caption="\n".join(text), parse_mode='HTML')
                    os.remove(f'{MEDIA_DIR}/{photo_name}')
        else:
            response = requests.get(url=url)
            if response.status_code == 200:
                html = response.text
                soup = BeautifulSoup(html, 'html.parser')
                div = soup.find_all('div', attrs={'class': 'left_col'})[1]
                post = div.find_all('li', attrs={'class': 'post_list'})[0]
                last_post_id = int(len(post.text))
                await db.create_theepochtimes(last_post_id)

    except KeyboardInterrupt:
        logging.info('theepochtimes выключен')
    except:
        await asyncio.sleep(15)
