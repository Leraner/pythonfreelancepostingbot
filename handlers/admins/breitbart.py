import asyncio
import logging
import os

import requests
from bs4 import BeautifulSoup

from data import database
from data.config import MEDIA_DIR, telegram_channel
from data.models import Breitbart
from loader import dp

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
    'Accept': '*/*'
}

db = database.DBCommands()


async def parsing_breitbart(url):
    try:
        last_post = await Breitbart.query.gino.first()
        if last_post:
            response = requests.get(url=url, headers=headers)
            if response.status_code == 200:
                html = response.text
                soup = BeautifulSoup(html, 'html.parser')
                artcieles = soup.find_all('article')
                post = artcieles[0]
                title = post.find_all('h2')[0].text
                post_id = int(post.contents[0].contents[2].contents[2].attrs['data-dsqi'])
                last_post_id = int(last_post.last_post_id)
                if last_post.before_last_post_id:
                    before_last_post_id = int(last_post.before_last_post_id)
                else:
                    before_last_post_id = last_post.before_last_post_id
                if post_id != last_post_id and post_id != before_last_post_id:
                    photo = post.contents[1].contents[0].attrs['src']
                    photo_name = photo.rsplit('/')[-1]
                    photo = requests.get(photo)
                    part_link = post.contents[1].attrs['href']
                    href = url + f'{part_link}'
                    with open(f'{MEDIA_DIR}/{photo_name}', 'wb') as f:
                        f.write(photo.content)
                        f.close()
                    await Breitbart.update.values(last_post_id=post_id, before_last_post_id=last_post_id).where(
                        Breitbart.id == 1).gino.first()
                    text = [
                        post.text,
                        f'<a href="{href}">SOURCE</a>'
                    ]
                    await dp.bot.send_photo(telegram_channel, photo=open(f'{MEDIA_DIR}/{photo_name}', 'rb'),
                                            caption="\n".join(text), parse_mode='HTML')
                    os.remove(f'{MEDIA_DIR}/{photo_name}')
        else:
            response = requests.get(url=url)
            if response.status_code == 200:
                html = response.text
                soup = BeautifulSoup(html, 'html.parser')
                artcieles = soup.find_all('article')
                last_post = artcieles[0]
                last_post_id = int(last_post.contents[0].contents[2].contents[2].attrs['data-dsqi'])
                await db.create_breitbart(last_post_id)

    except KeyboardInterrupt:
        logging.info('breitbart выключен')
    except:
        await asyncio.sleep(15)
