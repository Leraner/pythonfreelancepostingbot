import logging
import time

from parler import Parler
from loader import dp

from data.models import Parlers


async def parler(client):
    parler_accounts = await Parlers.query.gino.all()
    while True:
        try:
            for account in parler_accounts:
                current_account = client.userSearch(username=account.name)
                id = current_account['users'][0]['id']
                last_post = client.getPostsOfUserId(userId=id)['posts'][0]
                if last_post['_id'] != account.last_post_id:
                    body = last_post['body']
                    text = [
                        f'{account.name}',
                        f'{body}',
                    ]
                    await Parlers.update.values(last_post_id=last_post['_id']).where(Parlers.id == account.id).gino.first()
                    account.last_post_id = last_post['_id']
                    await dp.bot.send_message('@conservative_digest', text="\n".join(text))
                time.sleep(20)
        except KeyboardInterrupt:
            logging.info('Perler выключен')
