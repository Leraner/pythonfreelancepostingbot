from aiogram import types

from data import config
from loader import dp
from utils.misc import rate_limit


@rate_limit(5, 'help')
@dp.message_handler(user_id=config.ADMINS, commands=['help'])
async def bot_help(message: types.Message):
    text = [
        'Список команд: ',
        '/alltwitter - Список всех твиттер аккаунтов',
        '/deltwitter - Удалить твиттер аккаунт с определённых каналов или удалить полностью',
        '/addtwitter - Добавить твиттер аккаунт',
        '/addreddit - Добавить subreddit',
        '/delreddit - Удалить subreddit с определённых каналов или удалить полностью',
        '/allreddit - Список всех subreddit',
        '/changereddittime - Поменять время запощенных постов в subreddit',
        '/createtg - Добавть телеграм каналы',
        '/check - Список телеграм каналов и куда постятся какие subreddit/twitter аккаунты',
        '/help - Получить справку',
    ]
    await message.answer('\n'.join(text))
